<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<%@ page import="java.util.*"%>
<%@ page import="com.gatherlife.manager.utils.JspUtils"%>
<%@ page import="org.xson.common.object.XCO"%>
<%@ page import="com.gatherlife.manager.utils.NavTree"%>
<%

    response.setHeader("Cache-Control","no-store");  
    response.setDateHeader("Expires", 0);  
    response.setHeader("Pragma","no-cache");   

   // 获取token
 	String token = null;
 	if(JspUtils.getToken(request)!=null){
 		token = JspUtils.getToken(request);
 	}
 	System.out.println("---------tokentoken------"+token);
 	if(null == token){
 		response.sendRedirect("../../index.jsp");
 		return;
 	}
 	System.out.println("---------------"+token);
 	org.xson.common.object.XCO userXCO = JspUtils.getUserInfo(token);
 	if(null == userXCO){
 		response.sendRedirect("../../index.jsp");
 		return;
 	}
 	String real_name= userXCO.getStringValue("real_name");
 	long role= userXCO.getLongValue("role_id");
	
%>
<html>
<head>
	<link rel="shortcut icon" href="favicon.ico">
	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/style.css?v=4.1.0" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/js/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
	<!-- Data Tables -->
	<link href="${pageContext.request.contextPath}/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

	<!-- 全局js -->
	<script src="${pageContext.request.contextPath}/js/jquery.min.js?v=2.1.4"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/layer/layer.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/fancybox/jquery.fancybox.js"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/iCheck/icheck.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/plugins/peity/jquery.peity.min.js"></script>
	<!-- 自定义js -->
	<script src="${pageContext.request.contextPath}/js/hAdmin.js?v=4.1.0"></script>
	<script src="${pageContext.request.contextPath}/js/layer.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/index.js"></script>
	<script src="${pageContext.request.contextPath}/js/content.js?v=1.0.0"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/xco.js"></script>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/public.js"></script>
   	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-xco-src.js"></script>
	<SCRIPT src="${pageContext.request.contextPath}/js/xco.template.js" crossorigin="anonymous"></SCRIPT>
	<SCRIPT src="${pageContext.request.contextPath}/js/xco.databinding.js" crossorigin="anonymous"></SCRIPT>
	<!-- 第三方插件 -->
	<script src="${pageContext.request.contextPath}/js/plugins/pace/pace.min.js"></script>
	
	
</head>
<body>


</body>
</html>
